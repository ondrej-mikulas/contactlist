import React, { Component } from 'react'
import { View } from 'react-native'
import { Formik } from 'formik'
import * as Yup from 'yup'
import _ from 'lodash'
import TextInputDecorated from './TextInputDecorated'
import RoundedButton from '../../App/Components/RoundedButton'

// Styles
import styles from './Styles/InputFormStyle'

const validationSchema = Yup.object().shape({
  name: Yup.string()
    .required('Required field.')
    .min(5, 'At least 5 characters.'),
  phone: Yup.string()
    .required('Required field.')
    .min(5, 'At least 5 characters.')
})

// TODO prop onSubmit is required, how to transfer this condition to code?
// Maybe flow typing will helps here
export default class InputForm extends Component {
  render () {
    return (
      <Formik
        initialValues={{
          name: '',
          phone: ''
        }}
        onSubmit={values => {
          this.props.onSubmit(values)
        }}
        validationSchema={validationSchema}
        mapPropsToValues={({ formik: { errors, touched, setFieldValue, setFieldTouched, values, isSubmitting }, name, ...props }) => ({
          error: _.get(errors, name),
          touched: _.get(touched, name),
          ...props,
          name
        })}
        render={props => (
          <View style={styles.inputForm}>
            <TextInputDecorated
              name='name'
              onChangeText={props.handleChange('name')}
              error={props.touched.name && props.errors.name}
              touched={props.touched.name}
              value={props.values.name}
              placeholder='First and Last Name'
            />
            <TextInputDecorated
              name='phone'
              type='digits'
              onChangeText={props.handleChange('phone')}
              error={props.touched.phone && props.errors.phone}
              touched={props.touched.phone}
              value={props.values.phone}
              placeholder='Phone'
            />
            <RoundedButton onPress={props.handleSubmit}>
              Add
            </RoundedButton>
          </View>
        )}
      />
    )
  }
}
