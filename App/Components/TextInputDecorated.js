import React, { Component } from 'react'
import { View, Text, TextInput } from 'react-native'
import { compose, mapProps } from 'recompose'

// Styles
import styles from './Styles/TextInputDecoratedStyle'

const getInputTypeProps = type => {
  switch (type) {
    case 'email':
      return {
        autoCorrect: false,
        keyboardType: 'email-address',
        autoCapitalize: 'none'
      }
    case 'password':
      return {
        autoCorrect: false,
        secureTextEntry: true,
        autoCapitalize: 'none'
      }
    case 'digits':
      return {
        keyboardType: 'phone-pad'
      }
    case 'name':
      return {
        autoCorrect: false
      }
    default:
      return {}
  }
}

const withInputTypeProps = mapProps(({ type, ...props }) => ({
  ...getInputTypeProps(type),
  ...props
}))

class TextInputDecorated extends Component {
  render () {
    const { error, touched, ...props } = this.props
    const displayError = !!error && touched
    return (
      <View>
        <TextInput style={styles.inputBox}
          {...props}
        />
        <Text style={[ styles.errorText, displayError ? styles.redColor : styles.transparentColor ]}
        >
          {error}
        </Text>
      </View>
    )
  }
}

export default compose(withInputTypeProps)(TextInputDecorated)
