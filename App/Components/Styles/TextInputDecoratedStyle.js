import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  errorText: {
    textAlign: 'left',
    height: 20,
    backgroundColor: 'rgb(255, 255, 255)'
  },
  redColor: {
    color: 'rgb(239, 51, 64)'
  },
  transparentColor: {
    color: 'transparent'
  }
})
