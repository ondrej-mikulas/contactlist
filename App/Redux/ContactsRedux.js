import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
  getContactsRequest: null,
  getContactsSuccess: ['answer'],
  getContactsFailure: ['error'],
  getDetailsRequest: ['userId'],
  getDetailsSuccess: ['answer'],
  getDetailsFailure: ['error'],
  postContactRequest: ['contact'],
  postContactSuccess: ['answer'],
  postContactFailure: ['error']
})

export const ContactsTypes = Types
export default Creators

/* Initial State for contacts */
export const INITIAL_STATE_CONTACTS = Immutable({
  contacts: [],
  fetching: true,
  errorMessage: '',
  error: false
})

/* Initital State for details */
export const INITIAL_STATE_DETAILS = Immutable({
  details: [],
  fetching: true,
  errorMessage: '',
  error: false
})

/* Reducers */
export const getContactsRequest = (state, action) => {
  return state.merge({ fetching: true, error: false, errorMessage: '' })
}

export const getContactsSuccess = (state, action) => {
  return state.merge({ fetching: false, error: false, errorMessage: '', contacts: action.answer })
}

export const getContactsFailure = (state, action) => {
  return state.merge({ fetching: false, error: true, errorMessage: action.error })
}

export const postContactRequest = (state, {contact}) => {
  console.tron.display({ name: 'reducer post', value: contact })
  return state.merge({ fetching: true, error: false, errorMessage: '' })
}

export const postContactSuccess = (state, action) => {
  console.tron.display({value: action})
  return state.merge({ fetching: false, error: false, errorMessage: '', contacts: state.contacts.concat(action.answer) })
}

export const postContactFailure = (state, action) => {
  return state.merge({ fetching: false, error: true, errorMessage: action.error })
}

export const getDetailsRequest = (state, {userId}) => {
  console.tron.log('reducer ' + userId)
  return state.merge({ fetching: true, error: false, errorMessage: '' })
}

export const getDetailsSuccess = (state, action) => {
  return state.merge({ fetching: false, error: false, errorMessage: '', details: action.answer })
}

export const getDetailsFailure = (state, action) => {
  return state.merge({ fetching: false, error: true, errorMessage: action.error })
}

export const reducerContacts = createReducer(INITIAL_STATE_CONTACTS, {
  [Types.GET_CONTACTS_REQUEST]: getContactsRequest,
  [Types.GET_CONTACTS_SUCCESS]: getContactsSuccess,
  [Types.GET_CONTACTS_FAILURE]: getContactsFailure,
  [Types.POST_CONTACT_REQUEST]: postContactRequest,
  [Types.POST_CONTACT_SUCCESS]: postContactSuccess,
  [Types.POST_CONTACT_FAILURE]: postContactFailure
})

export const reducerDetails = createReducer(INITIAL_STATE_DETAILS, {
  [Types.GET_DETAILS_REQUEST]: getDetailsRequest,
  [Types.GET_DETAILS_SUCCESS]: getDetailsSuccess,
  [Types.GET_DETAILS_FAILURE]: getDetailsFailure
})
