import React, { Component } from 'react'
import { Images } from '../Themes'
import { KeyboardAvoidingView, Image, View } from 'react-native'
import CustomNavigationHeader from '../Navigation/CustomNavigationHeader'
import { connect } from 'react-redux'
import ContactsActions from '../Redux/ContactsRedux'
import InputForm from '../../App/Components/InputForm'

// Styles
import styles from './Styles/AddUserScreenStyle'

class AddUserScreen extends Component {
  static navigationOptions = () => {
    return {
      headerTitle: <CustomNavigationHeader pageTitle='Add New Contact' />
    }
  }

  onSubmit = (values) => {
    console.tron.display({ name: 'onSubmit from AddUserScreen', value: values })
    this.props.postContact(values)
  }

  render () {
    console.tron.display({name: 'props within Formik', value: this.props})
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <KeyboardAvoidingView behavior='padding'>
          <InputForm onSubmit={this.onSubmit} />
        </KeyboardAvoidingView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    contacts: state.contacts.contacts
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    postContact: (contact) => dispatch(ContactsActions.postContactRequest(contact))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddUserScreen)
