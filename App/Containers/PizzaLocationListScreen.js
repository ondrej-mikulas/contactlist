// @flow

import React, { Component } from 'react'
import { View, ScrollView, Text, Image, FlatList } from 'react-native'
import { ListItem } from 'react-native-elements'
import { connect } from 'react-redux'
import { Images } from '../Themes'
import RoundedButton from '../../App/Components/RoundedButton'
import ContactsActions from '../Redux/ContactsRedux'

// Styles
import styles from './Styles/PizzaLocationListScreenStyle'

class PizzaLocationListScreen extends Component {
  componentDidMount () {
    this.props.getAllContacts()
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    /*
    console.tron.display({name: 'prevProps', value: prevProps})
    console.tron.display({name: 'props', value: this.props})
    */
  }

  openPizzaLocation = () => {
    this.props.navigation.navigate('LaunchScreen', {
      itemId: '45',
      otherParam: 'sampleText'
    })
  }

  details = (userId) => {
    return () => {
      this.props.navigation.navigate('LaunchScreen', {
        userId: userId,
        otherParam: 'newText'
      })
    }
  }

  goBack = () => {
    this.props.navigation.goBack()
  }

  renderItem = ({item}) => (
    <ListItem
      button onPress={this.details(item.id)}
      title={<Text style={styles.titleText}>{item.name}</Text>}
      subtitle={<Text style={styles.titleText}>{item.phone}</Text>}
      roundAvatar
      avatar={{uri: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg'}}
    />
  )

  keyExtractor = (item, index) => item.id

  render () {
    // const avatar_url = 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg'
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <View style={styles.section}>
          <RoundedButton onPress={this.openPizzaLocation}>
            Launch Screen
          </RoundedButton>
          <RoundedButton onPress={this.goBack}>
            Go back
          </RoundedButton>
        </View>
        <ScrollView style={styles.container}>
          <FlatList
            data={this.props.cont.contacts}
            renderItem={this.renderItem}
            keyExtractor={this.keyExtractor}
          />
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    cont: state.contacts
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAllContacts: () => dispatch(ContactsActions.getContactsRequest())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PizzaLocationListScreen)
