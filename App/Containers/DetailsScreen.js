import React, { Component } from 'react'
import { ScrollView, Text, Image, View, FlatList } from 'react-native'
import { ListItem } from 'react-native-elements'
import { Images } from '../Themes'
import FullButton from '../../App/Components/FullButton'
import { connect } from 'react-redux'
import ContactsActions from '../Redux/ContactsRedux'
import CustomNavigationHeader from '../Navigation/CustomNavigationHeader'

// Styles
import styles from './Styles/DetailsScreenStyles'

class DetailsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <CustomNavigationHeader pageTitle={navigation.getParam('nameParam', 'NO-ID')} />
    }
  }

  componentDidMount () {
    this.props.getDetails(this.props.navigation.state.params.userId)
    const name = this.getContact(this.props.navigation.state.params.userId).name
    this.props.navigation.setParams({nameParam: name})
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    /*
    console.tron.display({name: 'didUpdate prevProps', value: this.props})
    console.tron.display({name: 'didUpdate props', value: this.props})
    */
  }

  openPizzaLocation = () => {
    this.props.navigation.navigate('LaunchScreen', {
      itemId: 47,
      otherParam: 'sampleText'
    })
  }

  goBack = () => {
    this.props.navigation.goBack()
  }

  renderItem = ({item}) => (
    <ListItem
      title={<Text style={styles.titleText}>{item.name}</Text>}
      subtitle={<Text style={styles.titleText}>{item.count}x</Text>}
    />
  )

  getContact (id) {
    return this.props.contacts.find(function (obj) { return obj.id === id })
  }

  keyExtractor = (item, index) => index.toString()

  render () {
    console.tron.log('Log after render.')
    console.tron.display({value: this.props})
    /*
    console.tron.display({name: 'getId', value: this.getContact(this.props.userId)})
    */
    const contact = this.getContact(this.props.navigation.state.params.userId)
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <FullButton text={'Phone: ' + contact.phone} />
        <ScrollView style={styles.container}>
          <FlatList
            data={this.props.details}
            renderItem={this.renderItem}
            keyExtractor={this.keyExtractor}
          />
        </ScrollView>
      </View>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    details: state.details.details,
    contacts: state.contacts.contacts
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getDetails: (userId) => dispatch(ContactsActions.getDetailsRequest(userId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailsScreen)
