import { createStackNavigator } from 'react-navigation'
import AddUserScreen from '../Containers/AddUserScreen'
import ContactsScreen from '../Containers/ContactsScreen'
import DetailsScreen from '../Containers/DetailsScreen'
import React from 'react'
import { Button, View } from 'react-native'
import styles from './Styles/NavigationStyles'
import CustomNavigationHeader from './CustomNavigationHeader'

const INITIAL_ROUTE_NAME = 'ContactsScreen'

const sharedNavigationOptions = ({ navigation }) => {
  if (navigation.state.routeName === INITIAL_ROUTE_NAME) {
    return {
      headerTitle: <CustomNavigationHeader pageTitle='Contacts' />,
      headerRight: (
        <View style={styles.navigationBarButton}>
          <Button
            onPress={() => navigation.navigate('AddUserScreen')}
            title='Add Contact'
            color='#555'
          />
        </View>
      )
    }
  }
  return {
    headerTitle: <CustomNavigationHeader pageTitle={navigation.state.routeName} />
  }
}

// Manifest of possible screens
const PrimaryNav = createStackNavigator({
  AddUserScreen: {
    screen: AddUserScreen
  },
  DetailsScreen: {
    screen: DetailsScreen
  },
  ContactsScreen: {
    screen: ContactsScreen,
    navigationOptions: sharedNavigationOptions
  }
}, {
  // Default config for all screens
  // headerMode: 'none',
  initialRouteName: 'ContactsScreen',
  navigationOptions: {
    headerStyle: {
      backgroundColor: 'red'
    }
  }
})

export default PrimaryNav
