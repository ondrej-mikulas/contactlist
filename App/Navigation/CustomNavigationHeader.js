import React from 'react'
import { Text, View, Image } from 'react-native'
import styles from './Styles/NavigationStyles'
import { Images } from '../Themes'

export default class CustomNavigationHeader extends React.Component {
  render () {
    return (
      <View style={{flexDirection: 'row'}}>
        <Image source={Images.hamburger} style={{ width: 40, height: 40, marginRight: 15, marginTop: 15, marginLeft: 15 }} />
        <Text style={styles.sectionText}>{this.props.pageTitle}</Text>
      </View>
    )
  }
}
