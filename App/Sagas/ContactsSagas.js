import { call, put } from 'redux-saga/effects'
import ContactsActions from '../Redux/ContactsRedux'

export function * getAllContacts (api, action) {
  try {
    const response = yield call(api.getContacts)
    if (response.ok) {
      // todo check !!! how does api call lookslike
      yield put(ContactsActions.getContactsSuccess(response.data.items))
    } else {
      yield put(ContactsActions.getContactsFailure('Connection problems :('))
    }
  } catch (error) {
    yield put(ContactsActions.getContactsFailure(error.message))
  }
}

export function * getDetails (api, action) {
  const { userId } = action
  try {
    const response = yield call(api.getDetails, userId)
    console.tron.display({name: 'fromSaga', value: response})
    if (response.ok) {
      yield put(ContactsActions.getDetailsSuccess(response.data.items))
    } else {
      yield put(ContactsActions.getDetailsFailure('Connection problems :('))
    }
  } catch (error) {
    console.tron.display({name: 'fromSaga', value: error})
    console.tron.display({name: 'fromSaga', value: error.message})
    yield put(ContactsActions.getDetailsFailure(error.message))
  }
}

export function * postContact (api, action) {
  console.tron.display({name: 'postContact saga', value: action})
  const { contact } = action
  try {
    const response = yield call(api.postContact, contact)
    console.tron.display({name: 'fromSaga', value: response})
    if (response.ok) {
      yield put(ContactsActions.postContactSuccess(contact))
    } else {
      yield put(ContactsActions.postContactFailure('Connection problems :('))
    }
  } catch (error) {
    yield put(ContactsActions.postContactFailure(error.message))
  }
}
