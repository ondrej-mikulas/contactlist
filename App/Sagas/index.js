import { takeLatest, all } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { GithubTypes } from '../Redux/GithubRedux'
import { ContactsTypes } from '../Redux/ContactsRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { getUserAvatar } from './GithubSagas'
import { getAllContacts, getDetails, postContact } from './ContactsSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const apiGithub = DebugConfig.useFixtures ? FixtureAPI : API.createGithubApi()
const apiContacts = API.createContactsApi()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),

    // some sagas receive extra parameters in addition to an action
    takeLatest(GithubTypes.USER_REQUEST, getUserAvatar, apiGithub),

    // contacts sagas
    takeLatest(ContactsTypes.GET_CONTACTS_REQUEST, getAllContacts, apiContacts),
    takeLatest(ContactsTypes.GET_DETAILS_REQUEST, getDetails, apiContacts),
    takeLatest(ContactsTypes.POST_CONTACT_REQUEST, postContact, apiContacts)
  ])
}
