declare module 'awesome-library' {
  declare type AwesomeType = {
    name: string,
    reason: string,
    is_awesome: boolean
  }
  declare function makeAwesome(name: string): AwesomeType
  declare function create<T>(obj: {[key: T]: any}): {[key: T]: number}
}
